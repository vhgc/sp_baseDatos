function datos(mongoose) {

	var Schema = mongoose.Schema;

	var jUsuario = new Schema({
		codigoEmpleado : String,
    	email : String,
    	prefijo : [String]
	});

	// Retornamos Nombre de la Coleccion, Datos que la Conforman
	var Modelo = mongoose.model('usuarios', jUsuario);
	return Modelo;
}

module.exports.getModeloUsuarios = datos;
