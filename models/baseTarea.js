function datos(mongoose) {

	var Schema = mongoose.Schema;

	var jTarea = new Schema({
		idproyecto : String,
		actividad : String,
		inicio : String,
		fin : String,
		valor : String,
		avance : String,
		completado : String,
		estatus : String
	});

	// Retornamos Nombre de la Coleccion, Datos que la Conforman
	var Modelo = mongoose.model('tareas', jTarea);
	return Modelo;
}

module.exports.getModeloTareas = datos;
